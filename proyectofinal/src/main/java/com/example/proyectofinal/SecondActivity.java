package com.example.proyectofinal;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class SecondActivity extends AppCompatActivity {

    GridView gridView;
    List<String> names = new ArrayList<>();
    List<Integer> images = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        gridView = findViewById(R.id.grid_view);

        fillArray();

        GripAdapter adapter = new GripAdapter(this,names,images);
        gridView.setAdapter(adapter);

        gridView.setOnItemClickListener((adapterView, view, i, l) -> {
            Toast.makeText(getApplicationContext(),names.get(i),Toast.LENGTH_SHORT).show();
            if(i == 0){
                Intent intent = new Intent(SecondActivity.this, AbecedaryActivity.class);
                String title = names.get(i);
                // intent.putExtra("TEXT", title);
                startActivity(intent);
            }
            if(i == 1){
                Intent intent = new Intent(SecondActivity.this, GreetingsActivity.class);
                String title = names.get(i);
                //intent.putExtra("TEXT", title);
                startActivity(intent);
            }
        });
    }

    private void fillArray(){
        names.add("Abecedario");
        names.add("Palabras");

        images.add(R.drawable.senas);
        images.add(R.drawable.palabras);

    }
}