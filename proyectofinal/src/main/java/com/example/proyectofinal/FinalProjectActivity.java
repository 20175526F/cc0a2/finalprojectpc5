package com.example.proyectofinal;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class FinalProjectActivity extends AppCompatActivity {

    Button enter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_final_project);

        enter = findViewById(R.id.btn_enter);

        Intent intent = new Intent(getApplicationContext(),SecondActivity.class);

        enter.setOnClickListener(view -> startActivity(intent));
    }
}